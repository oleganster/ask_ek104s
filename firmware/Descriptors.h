#ifndef _DESCRIPTORS_H_
#define _DESCRIPTORS_H_

#include <avr/pgmspace.h>			// PROGMEM - используется, чтобы сохранить переменную в flash памяти
#include <LUFA/Drivers/USB/USB.h>

/*
	Тип define для структуры дескриптора конфигурации устройства. 
	Это должно быть определено в коде приложения, поскольку дескриптор конфигурации содержит 
	несколько поддексоров, которые различаются для разных устройств, и которые описывают 
	использование устройства для хоста.
 */
typedef struct
{
	USB_Descriptor_Configuration_Header_t Config;
} USB_Descriptor_Configuration_t;

/*
	Перечисление для идентификаторов дескриптора строки устройства внутри устройства. 
	Каждый дескриптор строки должен иметь связанный с ним уникальный идентификатор ID, 
	который может использоваться для ссылки на строку из других дескрипторов.
*/
enum StringDescriptors_t
{
	STRING_ID_Language     = 0, /**< Supported Languages string descriptor ID (must be zero) */
	STRING_ID_Manufacturer = 1, /**< Manufacturer string ID */
	STRING_ID_Product      = 2, /**< Product string ID */
	STRING_ID_Serial       = 3, /**< Serial number string ID */
};

/* Function Prototypes: */
uint16_t CALLBACK_USB_GetDescriptor(const uint16_t wValue,
                                    const uint16_t wIndex,
                                    const void** const DescriptorAddress)
                                    ATTR_WARN_UNUSED_RESULT ATTR_NON_NULL_PTR_ARG(3);


#endif

