/*

TCNT0 - УС+
TCNT1 - МУ
TCNT3 - УС-

*/
#include "TERMINAL.h"

void SetupHardware(void)
{
	asm("cli");

	MCUSR &= ~(1 << WDRF);
	wdt_disable();
	clock_prescale_set(clock_div_1);

	USB_Init();
	setupAVR();

	asm("sei");
}

void setupAVR()
{

	/* US + */
	TCCR0A = 0x00;
	TCCR0B = _BV(CS02)|_BV(CS00); //CLK/1024 for test

	/* US - */
	TCCR3A = 0x00;
	TCCR3B = _BV(CS32)|_BV(CS30); //CLK/1024 for test

}

void EVENT_USB_Device_ControlRequest()
{
	if(((USB_ControlRequest.bmRequestType & CONTROL_REQTYPE_TYPE) == REQTYPE_VENDOR)
		&& ((USB_ControlRequest.bmRequestType & CONTROL_REQTYPE_RECIPIENT)== REQREC_DEVICE)){
		if ((USB_ControlRequest.bmRequestType & CONTROL_REQTYPE_DIRECTION)== REQDIR_HOSTTODEVICE){
			/*switch(USB_ControlRequest.bRequest){
				case SET_REQUEST_COM_CLEAN_READY_FLAG:
					Endpoint_ClearSETUP();
					Endpoint_ClearStatusStage();
					requestReady = false;
				break;
			}*/
		}
		else {
			switch(USB_ControlRequest.bRequest){
				case GET_REQUEST_COM_READ_BUFFER:
					inHOST();
				break;
			}
		}
	}
}


void inHOST(void)
{
	Endpoint_ClearSETUP();

	// Если готов передавать
	if(requestFlag){
		Endpoint_Write_Control_Stream_LE(buffer+BUFFER_PART_SIZE, BUFFER_PART_SIZE);
		requestFlag = false;
	}
	// Если нет, передадим 0 байт
	else{
		Endpoint_ClearIN();
	}

	Endpoint_ClearStatusStage();
}

void cleanTimers(){
	TCNT0 = 0;
	TCNT3 = 0;
}

int main(void)
{
	SetupHardware();
	GlobalInterruptEnable();

	currentStep = 0;
	lastStep = 0;
	test = 0;

	memset(buffer, 0, sizeof(buffer));

	requestFlag = false;

	while(1)
	{
		TCNT0 = 0;
		while(PIND & _BV(6)) USB_USBTask();
		buffer[currentStep++]= TCNT0;

		TCNT3 = 0;
		while(!(PIND & _BV(6))) USB_USBTask();
		buffer[currentStep++]= TCNT3;

		if(currentStep == BUFFER_PART_SIZE){
			requestFlag = true;
			memcpy(buffer+BUFFER_PART_SIZE, buffer, BUFFER_PART_SIZE);
			currentStep = 0;
		}

		if(test>=100) test=0;
	}
}	