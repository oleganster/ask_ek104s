/*
	Дескрипторы USB-устройства, для использования в библиотеке в режиме USB-устройства. 
	Дескрипторы представляют собой специальные машиночитаемые структуры, которые хост запрашивает 
	при перечислении устройств, чтобы определить возможности и функции устройства.
*/

#include "Descriptors.h"

/*
	Структура дескриптора устройства. Этот дескриптор, расположенный во флэш-памяти, 
	описывает общие характеристики устройства, включая поддерживаемую версию USB, 
	размер контрольной конечной точки и количество конфигураций устройств. 
	Дескриптор считывается USB-хостом, когда начинается процесс перечисления.
*/
const USB_Descriptor_Device_t PROGMEM DeviceDescriptor =
{
	.Header                 = {.Size = sizeof(USB_Descriptor_Device_t), .Type = DTYPE_Device},

	.USBSpecification       = VERSION_BCD(1,1,0),
	.Class                  = USB_CSCP_VendorSpecificClass,
	.SubClass               = USB_CSCP_NoDeviceSubclass,
	.Protocol               = USB_CSCP_NoDeviceProtocol,

	.Endpoint0Size          = FIXED_CONTROL_ENDPOINT_SIZE,

	.VendorID               = 0x16C0,
	.ProductID              = 0x05DC,
	.ReleaseNumber          = VERSION_BCD(2,0,0),

	.ManufacturerStrIndex   = STRING_ID_Manufacturer,
	.ProductStrIndex        = STRING_ID_Product,
	.SerialNumStrIndex      = STRING_ID_Serial,

	.NumberOfConfigurations = FIXED_NUM_CONFIGURATIONS
};

/*
	Структура дескриптора конфигурации. Этот дескриптор, расположенный во FLASH-памяти, 
	описывает использование устройства в одной из поддерживаемых конфигураций, включая 
	информацию об интерфейсах устройств и конечных точках. Дескриптор считывается хостом 
	USB во время процесса перечисления при выборе конфигурации, чтобы хост мог корректно 
	взаимодействовать с устройством USB.
*/
const USB_Descriptor_Configuration_t PROGMEM ConfigurationDescriptor =
{
	.Config =
		{
			.Header                 = {.Size = sizeof(USB_Descriptor_Configuration_Header_t), .Type = DTYPE_Configuration},

			.TotalConfigurationSize = sizeof(USB_Descriptor_Configuration_t),
			.TotalInterfaces        = 1,	//Всего интерфейсов

			.ConfigurationNumber    = 1,	//Номер конфигурации
			.ConfigurationStrIndex  = NO_DESCRIPTOR,

			.ConfigAttributes       = USB_CONFIG_ATTR_RESERVED,

			.MaxPowerConsumption    = USB_CONFIG_POWER_MA(500)
		},
};

/*
	Структура дескриптора языка. Этот дескриптор, расположенный во флэш-памяти, возвращается, 
	когда хост запрашивает дескриптор строки с индексом 0 (первый индекс). Это на самом деле массив 
	из 16-битных целых чисел, которые указывают через таблицу идентификаторов языков, доступную на USB.org, 
	какие языки поддерживает устройство для своих дескрипторов строк.
*/
const USB_Descriptor_String_t PROGMEM LanguageString =
{
	.Header                 = {.Size = USB_STRING_LEN(1), .Type = DTYPE_String},

	.UnicodeString          = {LANGUAGE_ID_ENG}
};

/*
	Строка дескриптора производителя. Это строка Unicode, содержащая данные изготовителя в удобочитаемой 
	форме и считывается по запросу хостом, когда запрашивается соответствующий идентификатор строки, 
	указанный в дескрипторе устройства.
*/
const USB_Descriptor_String_t PROGMEM ManufacturerString = USB_STRING_DESCRIPTOR(L"OASK");

/* 
	Строка дескриптора продукта. Это строка в Юникоде, содержащая сведения о продукте в удобочитаемой форме 
	и считываемая по запросу хостом, когда запрашивается соответствующий идентификатор строки, указанный в 
	дескрипторе устройства.
*/
const USB_Descriptor_String_t PROGMEM ProductString = USB_STRING_DESCRIPTOR(L"EK_TERMINAL");

/*

 */
const USB_Descriptor_String_t PROGMEM SerialString = USB_STRING_DESCRIPTOR(L"00001");

/* 
	Эта функция вызывается библиотекой в режиме устройства и должна быть переопределена 
	(см. Документацию библиотеки «Дескрипторы USB») кодом приложения, чтобы адрес и размер запрошенного 
	дескриптора могли быть переданы в библиотеку USB. Когда устройство получает запрос Get Descriptor на 
	конечной точке управления, эта функция вызывается так, что данные дескриптора могут быть переданы обратно, 
	а соответствующий дескриптор отправлен обратно на USB-хост.
*/
uint16_t CALLBACK_USB_GetDescriptor(const uint16_t wValue,
                                    const uint16_t wIndex,
                                    const void** const DescriptorAddress)
{
	const uint8_t  DescriptorType   = (wValue >> 8);
	const uint8_t  DescriptorNumber = (wValue & 0xFF);

	const void* Address = NULL;
	uint16_t    Size    = NO_DESCRIPTOR;

	switch (DescriptorType)
	{
		case DTYPE_Device:
			Address = &DeviceDescriptor;
			Size    = sizeof(USB_Descriptor_Device_t);
			break;
		case DTYPE_Configuration:
			Address = &ConfigurationDescriptor;
			Size    = sizeof(USB_Descriptor_Configuration_t);
			break;
		case DTYPE_String:
			switch (DescriptorNumber)
			{
				case STRING_ID_Language:
					Address = &LanguageString;
					Size    = pgm_read_byte(&LanguageString.Header.Size);
					break;
				case STRING_ID_Manufacturer:
					Address = &ManufacturerString;
					Size    = pgm_read_byte(&ManufacturerString.Header.Size);
					break;
				case STRING_ID_Product:
					Address = &ProductString;
					Size    = pgm_read_byte(&ProductString.Header.Size);
					break;
				case STRING_ID_Serial:
					Address = &SerialString;
					Size    = pgm_read_byte(&SerialString.Header.Size);
					break;
			}

			break;
	}

	*DescriptorAddress = Address;
	return Size;
}

