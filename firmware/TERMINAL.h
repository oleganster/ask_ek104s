#ifndef _RELAYBOARD_H_
#define _RELAYBOARD_H_

#include <avr/io.h>
#include <avr/wdt.h>
#include <avr/power.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include "Descriptors.h"
#include <LUFA/Drivers/USB/USB.h>

#define GET_REQUEST_COM_READ_BUFFER 0x02		// Команда для передачи на хост очередной посылки

#define BUFFER_SIZE 512
#define BUFFER_PART_SIZE BUFFER_SIZE/2

uint8_t buffer[BUFFER_SIZE];	// Буфер
bool requestFlag; 				// Флаг готовности передать FIFO_PART_SIZE байт
uint16_t currentStep;			// Текущий элемент массива для записи
uint16_t lastStep;				// Последений элемент массива в который производилась запись

int test;

void setupAVR(void);
void SetupHardware(void);
void inHOST(void);
void cleanTimers(void);
void EVENT_USB_Device_ControlRequest(void);

#endif

