#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>

#define DEV_VID 0x16C0 // Atmel code
#define DEV_PID 0x05DC

#define SET_REQUEST_COM_CLEAN_READY_FLAG 0x01	// Команда для снятия флага о готовногсти передавать данные
#define GET_REQUEST_COM_READ_BUFFER 0x02		// Команда для передачи на хост очередной посылки
#define REQUEST_STEP_MS 1

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // Настройка таблицы
    dataModel = new DataModel();
    ui->tableView->setModel(dataModel);
    ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableView->verticalHeader()->hide();
    ui->tableView->verticalScrollBarPolicy();
    connect(dataModel,SIGNAL(layoutChanged()),SLOT(actualScroll()));

    // Временная метка
    timeStep = 0;
    timeStamp.setHMS(0,0,0,0);

    // Таймер для испытания
    requestTimer = new QTimer();
    connect(requestTimer, SIGNAL(timeout()),SLOT(request()));

    // USBint time
    usbDev.setDebugLevel(2);

    if (usbDev.check(DEV_VID, DEV_PID)) ui->statusBar->showMessage(QString("connect USB"));
    else ui->statusBar->showMessage(QString("error connect"));

    error = 0;
    timeStampDebug = 0;
    tempTime = 0;

    tempStep = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
    delete dataModel;
    delete requestTimer;
}

void MainWindow::on_exitButton_clicked()
{
    close();
}

void MainWindow::actualScroll()
{
    ui->tableView->verticalScrollBar()->setValue(ui->tableView->verticalScrollBar()->maximum());
}

void MainWindow::request()
{
    tempStep++;

    int result = usbDev.read(GET_REQUEST_COM_READ_BUFFER);
    QList<qint8> getBuffer = usbDev.getDataBuffer();

    //qDebug ()<<"> request:"<<tempStep<<", result="<<result<<", data="<<getBuffer.length();

    if(result!=0){
        int temp = 0;


        for(int i=0; i<getBuffer.length()-1; i+=2){
            if(getBuffer[i]>30 or getBuffer[i+1]>30){
            TableData t;
            t.time = currentTestTime();
            t.sumP = getBuffer[i];
            t.sumM = getBuffer[i+1];
            /*if(t.sumM-t.sumP != 1 || temp-t.sumP > 99){
                t.error = "error";
                error++;
            }*/

            /*if(t.sumP>20 || t.sumM>20){
                t.error = "warning";
                error++;
            }*/

            dataModel->addTableData(t);
            //ui->statusBar->showMessage(QString("%0 warning").arg(error));
            ui->statusBar->showMessage(QString("%0").arg(t.time));
            }
        }
    }
}

QString MainWindow::currentTestTime()
{
    timeStamp = timeStamp.addMSecs(1);
    return timeStamp.toString("hh:mm:ss.zzz");
}


void MainWindow::on_startButton_clicked()
{
    requestTimer->start(REQUEST_STEP_MS);
}

void MainWindow::on_stopButton_clicked()
{
    requestTimer->stop();
}

QString MainWindow::convertBin(char byte)
{
    QString out = NULL;
    for(int i=7; i>=0; i--) out = out.append (QString("%0").arg((byte >> i)&(1 << 0)));
    return out;
}
