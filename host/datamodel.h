#ifndef DATAMODEL_H
#define DATAMODEL_H

#include <QAbstractTableModel>
#include <QTime>
#include <QList>

struct TableData {
    QString time;
    int     sumP;
    int     sumM;
    QString error;
};

class DataModel : public QAbstractTableModel
{

    Q_OBJECT

public:
    DataModel();
    ~DataModel();

    void addTableData(TableData t);
    int count();

private:
    int             rowCount(const QModelIndex &parent = QModelIndex()) const ;
    int             columnCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant        data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant        headerData(int section, Qt::Orientation orientation, int role) const;
    QList <TableData> tableData;
};

#endif // DATAMODEL_H
