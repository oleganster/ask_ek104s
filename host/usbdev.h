#ifndef USBDEV_H
#define USBDEV_H

#include <usb.h>
#include <QtGlobal>

#define DATA_BUFFER_SIZE 256
#define REQUES_TIMEOUT 1000

class USBDev
{
public:
    USBDev();
    ~USBDev();

    bool            check(qint32, qint32);
    void            cleanBuffer();
    void            setDebugLevel(int);
    void            write(qint8, qint8);
    QList<qint8>    getDataBuffer();
    int    read(qint8);

private:
    struct usb_bus      *bus;
    struct usb_device   *dev;

    usb_dev_handle  *usb_handle;
    qint8           buffer[DATA_BUFFER_SIZE];
};

#endif // USBDEV_H
