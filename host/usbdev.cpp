#include "usbdev.h"
#include <QDebug>

USBDev::USBDev()
{
    usb_handle = NULL;
}

USBDev::~USBDev()
{
    delete bus;
    delete dev;
    delete usb_handle;

}

void USBDev::cleanBuffer()
{
    memset(buffer, 0, sizeof(buffer));
}

bool USBDev::check(qint32 dev_vid, qint32 dev_pid)
{
    usb_init();
    usb_find_busses();  // Ищем шины
    usb_find_devices(); // Ищем устройства

    if (usb_handle) {
        usb_release_interface(usb_handle, 0);
        usb_close(usb_handle);
    }

    for(bus = usb_get_busses(); bus; bus = bus->next){
        for(dev = bus->devices; dev; dev = dev->next){
            int vid = dev->descriptor.idVendor;
            int pid = dev->descriptor.idProduct;


            if (vid == dev_vid && pid == dev_pid) {
                usb_handle = usb_open(dev);

                if (usb_handle) {

                    qDebug() << "USB connected";

                    // Reset
                    usb_reset(usb_handle);

                    // Запрос ядру на предоставление устройства
                    usb_detach_kernel_driver_np(usb_handle, 0);

                    // Запрос ядру на предоставление интерфейса
                    usb_claim_interface(usb_handle, 0);

                    return true;
                }
            }
        }
    }
    return false;
}

void USBDev::write(qint8 request, qint8 wValue)
{
    if ( usb_handle )
        usb_control_msg(
                    usb_handle,
                    USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_OUT,
                    request,
                    wValue,
                    0,
                    NULL,
                    0,
                    REQUES_TIMEOUT);
}

QList<qint8> USBDev::getDataBuffer()
{
    QList<qint8> temp;
    for(int i=0; i<sizeof(buffer); i++) temp << buffer[i];
    return temp;
}

int USBDev::read(qint8 request)
{
    if ( usb_handle ){
        int r = usb_control_msg(
                    usb_handle,
                    USB_TYPE_VENDOR | USB_RECIP_DEVICE | USB_ENDPOINT_IN,
                    request,
                    0,
                    0,
                    (char*)buffer,
                    sizeof(buffer),
                    REQUES_TIMEOUT);

        return r;
    }
}

void USBDev::setDebugLevel(int level)
{
    usb_set_debug(level);
}


