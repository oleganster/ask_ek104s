#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <datamodel.h>

#include <usbdev.h>
#include <stdint.h>
#include <stdio.h>
#include <errno.h>
#include <unistd.h>

#include <QTime>
#include <QTimer>

#include <QScrollBar>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    USBDev usbDev;
    QString convertBin(char byte);
    DataModel *dataModel;

    int timeStep;
    QTime timeStamp;
    QString currentTestTime();

    QTimer *requestTimer;

    long int error;
    long int timeStampDebug;
    long int tempTime;

    long tempStep;


private slots:
    void request();
    void on_startButton_clicked();
    void on_stopButton_clicked();
    void on_exitButton_clicked();
    void actualScroll();
};

#endif // MAINWINDOW_H

