#include "datamodel.h"

DataModel::DataModel()
{

}

DataModel::~DataModel()
{

}

int DataModel::rowCount(const QModelIndex&) const
{
    return tableData.length();
}

int DataModel::columnCount(const QModelIndex&) const
{
    return 5;
}

QVariant DataModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::DisplayRole){
         int row = index.row();
         int col = index.column();

            TableData  table = tableData.at(row);

            switch(col) {
                    case    0: return table.time;
                    case    1: return table.sumP;
                    case    2: return table.sumM;
                    case    3: return table.error;
                    case    4: return 0;

            }
    }

    if (role == Qt::TextAlignmentRole) return QVariant(Qt::AlignCenter);
    return QVariant();

}

QVariant DataModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole) return QVariant();

    if (orientation == Qt::Horizontal){
        switch(section){
            case 0: return QString("Время");
            case 1: return QString("∑+");
            case 2: return QString("∑-");
            case 3: return QString("Ошибки");
            case 4: return QString("-");
        }
        return QVariant();
    }

    else return section; //for vertical

}

void DataModel::addTableData(TableData t)
{
    tableData.append(t);

    emit    layoutAboutToBeChanged();   // собираюсь внести изменения в таблицу!!
    emit    layoutChanged();            // внести изменения
}

int DataModel::count()
{
    return tableData.length();
}
