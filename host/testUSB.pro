#-------------------------------------------------
#
# Project created by QtCreator 2017-03-27T13:45:31
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = testUSB
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    usbdev.cpp \
    datamodel.cpp
unix:LIBS += -lusb

HEADERS  += mainwindow.h \
    usbdev.h \
    datamodel.h

FORMS    += mainwindow.ui
